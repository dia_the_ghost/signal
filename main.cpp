#include <QApplication>
#include "MyWindow.h"

int main(int argc, char **argv)
{
    QApplication a(argc, argv);

    MyWindow *window = new MyWindow;
    window->show();

    str *n = new str;
    QObject::connect(window, SIGNAL(Simple(QString)), n, SLOT(Simple(QString)));
    QObject::connect(window, SIGNAL(Invers(QString)), n, SLOT(Inversion(QString)));
    QObject::connect(window,SIGNAL(Register(QString)), n, SLOT(Register(QString)));
    QObject::connect(window, SIGNAL(RegVers(QString)), n, SLOT(RegVers(QString)));

    return a.exec();
}
