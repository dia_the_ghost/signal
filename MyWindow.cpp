#include "MyWindow.h"

MyWindow::MyWindow(QWidget *parent) : QDialog(parent)  //�������� ����������� �������� ������ QDialog
{
    lbl = new QLabel("&Enter ");
    line = new QLineEdit;
    lbl->setBuddy(line);

    cb1 = new QCheckBox("&Uppercase");
    cb2 = new QCheckBox("&Inversion");

    ok = new QPushButton("&ok");
    ok->setDefault(true);
    ok->setEnabled(false);
    close = new QPushButton("&close");

    //������� ������ �������������� ���� � lbl � line
    QHBoxLayout *layout = new QHBoxLayout;
    layout->addWidget(lbl);
    layout->addWidget(line);

    //������� ������������ ���� � 1 ����� � cb1, cb2
    QVBoxLayout *right = new QVBoxLayout;
    right->addLayout(layout);             //��������� ���� �� ����
    right->addWidget(cb1);
    right->addWidget(cb2);

    //������� ������ ������������ ���� ����� �� 2� ������ �� � close
    QVBoxLayout *left = new QVBoxLayout;
    left->addWidget(ok);
    left->addWidget(close);

    //����� �������������� ���� �� ������������ ������� � ������� �����
    QHBoxLayout *main = new QHBoxLayout;
    main->addLayout(right);
    main->addLayout(left);

    connect(line, SIGNAL(textChanged(QString)), this, SLOT(TextChanged(QString))); //���������� ����� TextChange ��� line
    connect(close, SIGNAL(clicked()), this, SLOT(close()));
    connect(ok, SIGNAL(clicked()), this, SLOT(OkClicked()));

    //�������� ����� ���� � ����
    setLayout(main);
}

MyWindow::~MyWindow()
{
    //dtor
}

void MyWindow::TextChanged(QString str)
{
    ok->setEnabled(!str.isEmpty());
}

void MyWindow::OkClicked()
{
    if((cb1->isChecked()) && (cb2->isChecked()))
        emit RegVers(line->text());

    else if((!cb1->isChecked()) && (!cb2->isChecked()))
        emit Simple(line->text());

    else if(cb1->isChecked())
        emit Register(line->text());

    else if(cb2->isChecked())
        emit Invers(line->text());
}
