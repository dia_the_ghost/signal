#ifndef MYWINDOW_H
#define MYWINDOW_H

#include <QDialog>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include <QPushButton>
#include <QMessageBox>
                                //слои форматирования
#include <QHBoxLayout>
#include <QVBoxLayout>

class MyWindow : public QDialog
{
    Q_OBJECT
public:
    MyWindow(QWidget *parent = 0); //конструктор
    virtual ~MyWindow();
private:
    QLabel *lbl;
    QLineEdit *line;
    QCheckBox *cb1;
    QCheckBox *cb2;
    QPushButton *ok;
    QPushButton *close;
private slots:
    void OkClicked();
    void TextChanged(QString str);
signals:
    void Register(QString str); //
    void Invers (QString str); //
    void Simple (QString str);
    void RegVers (QString str);
};

class str : public QObject
{
    Q_OBJECT

private:
    QString Str;
public slots:
    void Simple(QString str)
    {
        QMessageBox msg;
        msg.setText(str);
        msg.exec();

    }
    void Inversion(QString str)
    {
        QString result = str;
        for(int i=str.size(), j=0; i>=0; i--, j++)
        {
            result[j] = str[i];
        }
        QMessageBox msg;
        msg.setText(result);
        msg.exec();
    }
    void Register(QString str)
    {
        str = str.toUpper();

        QMessageBox msg;
        msg.setText(str);
        msg.exec();
    }
    void RegVers(QString str)
    {
        QString result = str;
        for(int i=str.size(), j=0; i>=0; i--, j++)
        {
            result[j] = str[i];
        }
         result = result.toUpper();

         QMessageBox msg;
         msg.setText(result);
         msg.exec();
    }
};

#endif // MYWINDOW_H
